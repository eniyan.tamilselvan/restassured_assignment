package org.Demo;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class DeleteMethod {
	@Test
	public void f() {
		
		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
				.header("authorization", "Bearer 2854d8b8e888822710f788dcc1fe481a5ae9377bc01c7d60707da874c38faa65")
				.delete("/users/ 199848").then().statusCode(204);
	}
 
}