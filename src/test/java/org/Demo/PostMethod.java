package org.Demo;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import io.restassured.response.ValidatableResponse;

public class PostMethod {
	@Test
	public void testingPostOperation() {
		JSONObject req = new JSONObject();
		req.put("name", "Eniyan");
		req.put("email", "eniyan24@gmail.com");
		req.put("gender", "male");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";
		ValidatableResponse Response = given().log().all().contentType("application/json")
				.header("authorization", "Bearer 2854d8b8e888822710f788dcc1fe481a5ae9377bc01c7d60707da874c38faa65")
				.body(req.toJSONString()).when().post("/users").then().statusCode(201);
	
	}
 
}