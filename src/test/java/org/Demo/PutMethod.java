package org.Demo;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

import org.json.simple.JSONObject;

public class PutMethod {
	 @Test
	  public void testPutOperation() {
		  
		  JSONObject req = new JSONObject();
		  req.put("name", "Eni");
		  req.put("email", "eni123@gmail.com");
		  req.put("gender", "male");
		  req.put("status", "Active");
			
			baseURI = "https://gorest.co.in/public/v2";
			given().log().all().contentType("application/json")
			.header("authorization", "Bearer 2854d8b8e888822710f788dcc1fe481a5ae9377bc01c7d60707da874c38faa65")
			.body(req.toJSONString()).patch("/users/ 199848").then().statusCode(200);
	 	  
	  }
  
}
